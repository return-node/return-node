/*!
This module contains a function to return a node.
**/

use node::None;//The node type to be returned.

/// Returns a node.
fn return_node() -> Node {
    let node = Node::new();//the node to be returned
    return node;//return node
}
